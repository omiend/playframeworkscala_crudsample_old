package models

import play.api.db._
import anorm._
import play.api.Play.current
import anorm.SqlParser._
import java.util.Date

/**
 * USERモデルクラス
 */
case class USER(
  id: Long,
  sei: String,
  mei: String,
  seiKana: String,
  meiKana: String,
  birthdate: String,
  gender: String,
  detail: String) {
}

/** USERオブジェクト */
object USER {
  
  val data = {
    get[Long]("id") ~
    get[String]("sei") ~
    get[String]("mei") ~
    get[String]("seiKana") ~
    get[String]("meiKana") ~
    get[String]("birthdate") ~
    get[String]("gender") ~
    get[String]("detail") map {
      case id ~ sei ~ mei ~ seiKana ~ meiKana ~ birthdate ~ gender ~ detail => 
      USER(id, sei, mei, seiKana, meiKana, birthdate, gender, detail)
    }
  }
  
  /** 全件の件数取得 */
  def getByOffsetAll(offset: Int, limit: Int): List[USER] = {
    DB.withConnection { implicit c =>
      val dataList = SQL("Select * from USER limit " + limit + " offset " + offset).as(USER.data *)
      return dataList
    }
  }
  
  /** 件数取得 */
  def getCount(): Integer = {
    DB.withConnection { implicit c =>
      val count: Long = SQL("select count(*) from USER").as(scalar[Long].single)
      return count.toInt
    }
  }
  
  /** USERデータinsertメソッド */
  def insert(user: USER): USER = {
    DB.withConnection { implicit c =>
      val id: Int = SQL(
        """
          insert into user (
             sei
            ,mei
            ,seiKana
            ,meiKana
            ,birthdate
            ,gender
            ,detail
          )
          values (
             {sei}
            ,{mei}
            ,{seiKana}
            ,{meiKana}
            ,{birthdate}
            ,{gender}
            ,{detail}
          )
        """
      ).on(
        'sei -> user.sei,
        'mei -> user.mei,
        'seiKana -> user.seiKana,
        'meiKana -> user.meiKana,
        'birthdate -> user.birthdate,
        'gender -> user.gender,
        'detail -> user.detail
      ).executeUpdate()
    }
    user
  }
  
  /** USERデータID指定１件取得 */
  def getById(id:String): USER = {
    DB.withConnection { implicit c =>
      val result = SQL("Select * from USER where id = " + id).as(USER.data.single)
      return result
    }
  }
  
  /** USERデータupdateメソッド */
  def update(id:String, user: USER): USER = {
    DB.withConnection { implicit c =>
      val ids: Int = SQL(
        """
          update USER 
             set  sei={sei}
                 ,mei={mei}
                 ,seiKana={seiKana}
                 ,meiKana={meiKana}
                 ,birthdate={birthdate}
                 ,gender={gender}
                 ,detail={detail}
          where id = {id}
        """
      ).on(
        'id -> id,
        'sei -> user.sei,
        'mei -> user.mei,
        'seiKana -> user.seiKana,
        'meiKana -> user.meiKana,
        'birthdate -> user.birthdate,
        'gender -> user.gender,
        'detail -> user.detail
      ).executeUpdate()
    }
    user
  }
  
  /** USERデータdeleteメソッド */
  def delete(id: String) {
    DB.withConnection { implicit c =>
      val ids: Int = SQL(
        """
          delete from USER
           where id = {id}
        """
      ).on(
        'id -> id
      ).executeUpdate()
    }
  }
}
