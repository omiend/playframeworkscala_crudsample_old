package controllers

import models._
import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Form
import play.api.data.Forms._

trait TraitController extends Controller {
    /** データ取得用オフセット初期値 */
    var offset: Int = 0
    /** データ取得用リミット初期値 */
    var limit: Int = 10
    
}
