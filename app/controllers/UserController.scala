package controllers

import models._
import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Form
import play.api.data.Forms._

object UserController extends TraitController {

  // 対象モデルの項目を指定してFormを作成する
  val myForm = Form(
    mapping(
      "id" -> ignored(0L),
      "sei" -> nonEmptyText,
      "mei" -> nonEmptyText,
      "seiKana" -> text,
      "meiKana" -> text,
      "birthdate" -> text,
      "gender" -> text,
      "detail" -> text)(USER.apply)(USER.unapply))

  /** ユーザー一覧画面に遷移 */
  def indexUser = Action {
    val title = "ユーザー一覧"
    val message = "" // Stringで文字列を指定すると、メッセージボックスが表示される
    val messageType = "" // 下記の文字列ごとにメッセージボックスのタイプ（色）を指定出来る
    // val messageType = "alert" // 黄色のメッセージ
    // val messageType = "alert alert-error" // 赤色のメッセージ
    // val messageType = "alert alert-success" // 緑色のメッセージ
    // val messageType = "alert alert-info" // 青色のメッセージ
    val dataList = USER.getByOffsetAll(offset, limit)
    Ok(views.html.indexUser(title, message, messageType, dataList))
  }

  /** ユーザー登録画面に遷移 */
  def createUser = Action {
    val title = "ユーザー新規登録"
    val message = ""
    val messageType = ""
    Ok(views.html.createUser(title, message, messageType, myForm))
  }

  /** ユーザー登録処理 */
  def insertUser = Action { implicit request =>
    myForm.bindFromRequest.fold(
      errors => {
        val title = "ユーザー新規登録"
        val message = "ユーザー新規登録エラー"
        val messageType = "alert alert-error"
        BadRequest(views.html.createUser(title, message, messageType, errors))
      },
      form => {
        var tmpForm = myForm.bindFromRequest
        val user: USER = tmpForm.get
        USER.insert(user)
        val title = "ユーザー新規登録"
        val message = "ユーザー新規登録完了"
        val messageType = "alert alert-success"
        Ok(views.html.createUser(title, message, messageType, tmpForm))
      })
  }

  /** ユーザー更新画面に遷移 */
  def editUser(id: String) = Action { implicit request =>
    val user = USER.getById(id)
    val title = "ユーザー更新"
    val message = ""
    val messageType = "alert alert-success"
    Ok(views.html.editUser(title, message, messageType, myForm.fill(user), id))
  }

  /** ユーザー更新処理 */
  def updateUser(id: String) = Action { implicit request =>
    myForm.bindFromRequest.fold(
      errors => {
        val title = "ユーザー更新"
        val message = "ユーザー更新エラー"
        val messageType = "alert alert-error"
        BadRequest(views.html.editUser(title, message, messageType, errors, id))
      },
      form => {
        var tmpForm = myForm.bindFromRequest
        val user: USER = tmpForm.get
        USER.update(id, user)
        val title = "ユーザー更新"
        val message = "ユーザー更新完了"
        val messageType = "alert alert-success"
        Ok(views.html.editUser(title, message, messageType, tmpForm, id))
      })
  }

  /** ユーザー削除処理 */
  def deleteUser(id: String) = Action { implicit request =>
    USER.delete(id)
    val title = "ユーザー一覧"
    val message = "ユーザー削除完了"
    val messageType = "alert alert-success"
    val dataList = USER.getByOffsetAll(offset, limit)
    Ok(views.html.indexUser(title, message, messageType, dataList))
  }

  /** ----------------------------------------------------------------------------------------------------- */
  /** --- ページング処理 ---------------------------------------------------------------------------------- */
  /** ----------------------------------------------------------------------------------------------------- */
  def firstUser = Action {
    offset = 0
    val title = "ユーザー一覧"
    val dataList = USER.getByOffsetAll(offset, limit)
    val message = ""
    val messageType = ""
    Ok(views.html.indexUser(title, message, messageType, dataList))
  }

  def prevUser = Action {
    offset -= limit
    if (offset < 0) {
      offset = 0
    }
    val title = "ユーザー一覧"
    val dataList = USER.getByOffsetAll(offset, limit)
    val message = ""
    val messageType = ""
    Ok(views.html.indexUser(title, message, messageType, dataList))
  }

  def nextUser = Action {
    offset += limit
    if (offset >= USER.getCount()) {
      offset = USER.getCount() - 10
    }
    val title = "ユーザー一覧"
    val dataList = USER.getByOffsetAll(offset, limit)
    val message = ""
    val messageType = ""
    Ok(views.html.indexUser(title, message, messageType, dataList))
  }

  def lastUser = Action {
    offset = USER.getCount() - 10
    val title = "ユーザー一覧"
    val dataList = USER.getByOffsetAll(offset, limit)
    val message = ""
    val messageType = ""
    Ok(views.html.indexUser(title, message, messageType, dataList))
  }
}
