import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "EventBBS"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    // mysqlを利用する場合、下記の１行を記述
    "mysql" % "mysql-connector-java" % "5.1.20",
    jdbc,
    anorm
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here      
  )

}
