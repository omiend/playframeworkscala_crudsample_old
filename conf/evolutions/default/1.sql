# --- First database schema
 
# --- !Ups
create table USER (
    id        int(11) PRIMARY KEY AUTO_INCREMENT,
    sei       varchar(128),
    mei       varchar(128),
    seiKana   varchar(128),
    meiKana   varchar(128),
    birthdate varchar(128),
    gender    varchar(128),
    detail    varchar(128)
);

insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (1, 'sei1', 'mei', 'seiKana', 'meiKana', '1983-01-01', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (2, 'sei2', 'mei', 'seiKana', 'meiKana', '1983-01-02', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (3, 'sei3', 'mei', 'seiKana', 'meiKana', '1983-01-03', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (4, 'sei4', 'mei', 'seiKana', 'meiKana', '1983-01-04', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (5, 'sei5', 'mei', 'seiKana', 'meiKana', '1983-01-05', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (6, 'sei6', 'mei', 'seiKana', 'meiKana', '1983-01-06', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (7, 'sei7', 'mei', 'seiKana', 'meiKana', '1983-01-07', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (8, 'sei8', 'mei', 'seiKana', 'meiKana', '1983-01-08', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (9, 'sei9', 'mei', 'seiKana', 'meiKana', '1983-01-09', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (10, 'sei10', 'mei', 'seiKana', 'meiKana', '1983-01-10', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (11, 'sei11', 'mei', 'seiKana', 'meiKana', '1983-01-11', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (12, 'sei12', 'mei', 'seiKana', 'meiKana', '1983-01-12', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (13, 'sei13', 'mei', 'seiKana', 'meiKana', '1983-01-13', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (14, 'sei14', 'mei', 'seiKana', 'meiKana', '1983-01-14', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (15, 'sei15', 'mei', 'seiKana', 'meiKana', '1983-01-15', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (16, 'sei16', 'mei', 'seiKana', 'meiKana', '1983-01-16', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (17, 'sei17', 'mei', 'seiKana', 'meiKana', '1983-01-17', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (18, 'sei18', 'mei', 'seiKana', 'meiKana', '1983-01-18', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (19, 'sei19', 'mei', 'seiKana', 'meiKana', '1983-01-19', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (20, 'sei20', 'mei', 'seiKana', 'meiKana', '1983-01-20', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (21, 'sei21', 'mei', 'seiKana', 'meiKana', '1983-01-21', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (22, 'sei22', 'mei', 'seiKana', 'meiKana', '1983-01-22', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (23, 'sei23', 'mei', 'seiKana', 'meiKana', '1983-01-23', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (24, 'sei24', 'mei', 'seiKana', 'meiKana', '1983-01-24', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (25, 'sei25', 'mei', 'seiKana', 'meiKana', '1983-01-25', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (26, 'sei26', 'mei', 'seiKana', 'meiKana', '1983-01-26', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (27, 'sei27', 'mei', 'seiKana', 'meiKana', '1983-01-27', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (28, 'sei28', 'mei', 'seiKana', 'meiKana', '1983-01-28', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (29, 'sei29', 'mei', 'seiKana', 'meiKana', '1983-01-29', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (30, 'sei30', 'mei', 'seiKana', 'meiKana', '1983-01-30', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (31, 'sei31', 'mei', 'seiKana', 'meiKana', '1983-01-31', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (32, 'sei32', 'mei', 'seiKana', 'meiKana', '1983-02-01', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (33, 'sei33', 'mei', 'seiKana', 'meiKana', '1983-02-02', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (34, 'sei34', 'mei', 'seiKana', 'meiKana', '1983-02-03', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (35, 'sei35', 'mei', 'seiKana', 'meiKana', '1983-02-04', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (36, 'sei36', 'mei', 'seiKana', 'meiKana', '1983-02-05', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (37, 'sei37', 'mei', 'seiKana', 'meiKana', '1983-02-06', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (38, 'sei38', 'mei', 'seiKana', 'meiKana', '1983-02-07', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (39, 'sei39', 'mei', 'seiKana', 'meiKana', '1983-02-08', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (40, 'sei40', 'mei', 'seiKana', 'meiKana', '1983-02-09', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (41, 'sei41', 'mei', 'seiKana', 'meiKana', '1983-02-10', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (42, 'sei42', 'mei', 'seiKana', 'meiKana', '1983-02-11', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (43, 'sei43', 'mei', 'seiKana', 'meiKana', '1983-02-12', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (44, 'sei44', 'mei', 'seiKana', 'meiKana', '1983-02-13', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (45, 'sei45', 'mei', 'seiKana', 'meiKana', '1983-02-14', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (46, 'sei46', 'mei', 'seiKana', 'meiKana', '1983-02-15', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (47, 'sei47', 'mei', 'seiKana', 'meiKana', '1983-02-16', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (48, 'sei48', 'mei', 'seiKana', 'meiKana', '1983-02-17', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (49, 'sei49', 'mei', 'seiKana', 'meiKana', '1983-02-18', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (50, 'sei50', 'mei', 'seiKana', 'meiKana', '1983-02-19', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (51, 'sei51', 'mei', 'seiKana', 'meiKana', '1983-02-20', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (52, 'sei52', 'mei', 'seiKana', 'meiKana', '1983-02-21', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (53, 'sei53', 'mei', 'seiKana', 'meiKana', '1983-02-22', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (54, 'sei54', 'mei', 'seiKana', 'meiKana', '1983-02-23', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (55, 'sei55', 'mei', 'seiKana', 'meiKana', '1983-02-24', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (56, 'sei56', 'mei', 'seiKana', 'meiKana', '1983-02-25', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (57, 'sei57', 'mei', 'seiKana', 'meiKana', '1983-02-26', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (58, 'sei58', 'mei', 'seiKana', 'meiKana', '1983-02-27', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (59, 'sei59', 'mei', 'seiKana', 'meiKana', '1983-02-28', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (60, 'sei60', 'mei', 'seiKana', 'meiKana', '1983-03-01', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (61, 'sei61', 'mei', 'seiKana', 'meiKana', '1983-03-02', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (62, 'sei62', 'mei', 'seiKana', 'meiKana', '1983-03-03', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (63, 'sei63', 'mei', 'seiKana', 'meiKana', '1983-03-04', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (64, 'sei64', 'mei', 'seiKana', 'meiKana', '1983-03-05', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (65, 'sei65', 'mei', 'seiKana', 'meiKana', '1983-03-06', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (66, 'sei66', 'mei', 'seiKana', 'meiKana', '1983-03-07', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (67, 'sei67', 'mei', 'seiKana', 'meiKana', '1983-03-08', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (68, 'sei68', 'mei', 'seiKana', 'meiKana', '1983-03-09', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (69, 'sei69', 'mei', 'seiKana', 'meiKana', '1983-03-10', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (70, 'sei70', 'mei', 'seiKana', 'meiKana', '1983-03-11', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (71, 'sei71', 'mei', 'seiKana', 'meiKana', '1983-03-12', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (72, 'sei72', 'mei', 'seiKana', 'meiKana', '1983-03-13', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (73, 'sei73', 'mei', 'seiKana', 'meiKana', '1983-03-14', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (74, 'sei74', 'mei', 'seiKana', 'meiKana', '1983-03-15', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (75, 'sei75', 'mei', 'seiKana', 'meiKana', '1983-03-16', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (76, 'sei76', 'mei', 'seiKana', 'meiKana', '1983-03-17', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (77, 'sei77', 'mei', 'seiKana', 'meiKana', '1983-03-18', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (78, 'sei78', 'mei', 'seiKana', 'meiKana', '1983-03-19', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (79, 'sei79', 'mei', 'seiKana', 'meiKana', '1983-03-20', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (80, 'sei80', 'mei', 'seiKana', 'meiKana', '1983-03-21', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (81, 'sei81', 'mei', 'seiKana', 'meiKana', '1983-03-22', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (82, 'sei82', 'mei', 'seiKana', 'meiKana', '1983-03-23', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (83, 'sei83', 'mei', 'seiKana', 'meiKana', '1983-03-24', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (84, 'sei84', 'mei', 'seiKana', 'meiKana', '1983-03-25', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (85, 'sei85', 'mei', 'seiKana', 'meiKana', '1983-03-26', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (86, 'sei86', 'mei', 'seiKana', 'meiKana', '1983-03-27', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (87, 'sei87', 'mei', 'seiKana', 'meiKana', '1983-03-28', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (88, 'sei88', 'mei', 'seiKana', 'meiKana', '1983-03-29', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (89, 'sei89', 'mei', 'seiKana', 'meiKana', '1983-03-30', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (90, 'sei90', 'mei', 'seiKana', 'meiKana', '1983-03-31', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (91, 'sei91', 'mei', 'seiKana', 'meiKana', '1983-04-01', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (92, 'sei92', 'mei', 'seiKana', 'meiKana', '1983-04-02', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (93, 'sei93', 'mei', 'seiKana', 'meiKana', '1983-04-03', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (94, 'sei94', 'mei', 'seiKana', 'meiKana', '1983-04-04', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (95, 'sei95', 'mei', 'seiKana', 'meiKana', '1983-04-05', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (96, 'sei96', 'mei', 'seiKana', 'meiKana', '1983-04-06', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (97, 'sei97', 'mei', 'seiKana', 'meiKana', '1983-04-07', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (98, 'sei98', 'mei', 'seiKana', 'meiKana', '1983-04-08', 'F', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (99, 'sei99', 'mei', 'seiKana', 'meiKana', '1983-04-09', 'M', 'detail');
insert into USER (id, sei, mei, seiKana, meiKana, birthdate, gender, detail) values (100, 'sei100', 'mei', 'seiKana', 'meiKana', '1983-04-10', 'F', 'detail');

# --- !Downs

drop table USER;